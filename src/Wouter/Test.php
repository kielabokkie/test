<?php

namespace Wouter;

use Marvin\Modules\BaseModule;

class Test extends BaseModule
{
    public function trigger()
    {
        $this->trigger = 'YOUR_TRIGGER';
    }

    public function description()
    {
        $this->addDescriptionLine($this->trigger, 'YOUR_DESCRIPTION');
    }

    public function execute($parameters)
    {
        return $this->reply('YOUR_REPLY');
    }
}
